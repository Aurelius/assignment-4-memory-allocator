//
// Created by aurelius on 18.12.22.
//
#include "test.h"

enum {
    INITIAL_HEAP_SIZE = 10000,
    FIRST_BLOCK_SIZE = 374,
    SECOND_BLOCK_SIZE = 333,
    THIRD_BLOCK_SIZE = 555,
    FOURTH_BLOCK_SIZE = 777,
    FIFTH_BLOCK_SIZE = 888,
    EXTENDING_BLOCK_SIZE = 3000000,
    BLOCK_FOR_ANOTHER_PLACE_SIZE = 10000000
};

void simple_allocation(struct block_header* heap, size_t allocation_size) {
    printf("<------- TEST 1 ------->\n");
    printf("Обычное успешное выделение памяти.\n");

    void* mem = _malloc(allocation_size);
    if (mem == NULL) {
        err("Test 1 failed! :(\n");
    }

    debug_heap(stdout, heap);
    if (heap->capacity.bytes != allocation_size || heap->is_free != false) {
        err("Memory size does not match to magic number or not allocated!\n");
    }
    printf("Test 1 passed!\n\n\n");
    _free(mem);
}

void free_one_block(struct block_header* heap) {
    printf("<------- TEST 2 ------->\n");
    printf("Освобождение одного блока из нескольких выделенных.\n");

    void *first = _malloc(FIRST_BLOCK_SIZE);
    _malloc(SECOND_BLOCK_SIZE);
    _malloc(THIRD_BLOCK_SIZE);

    printf("Heap before\n");
    debug_heap(stdout, heap);
    _free(first);
    printf("Heap after\n");
    debug_heap(stdout, heap);

    printf("Test 2 passed!\n\n\n");
}

void free_two_blocks(struct block_header* heap) {
    printf("<------- TEST 3 ------->\n");
    printf("Освобождение двух блоков из нескольких выделенных.\n");

    void *first_block = _malloc(FIRST_BLOCK_SIZE);
    void *second_block = _malloc(SECOND_BLOCK_SIZE);
    _malloc(FIFTH_BLOCK_SIZE);
    printf("Heap before\n");
    debug_heap(stdout, heap);
    _free(second_block);
    printf("After free second block\n");
    debug_heap(stdout, heap);
    _free(first_block);
    printf("After free first block\n");
    debug_heap(stdout, heap);
    printf("Test 3 passed!\n\n\n");
}


void new_region_extends_old_region(struct block_header* heap) {
    printf("<------- TEST 4 ------->\n");
    printf("Память закончилась, новый регион памяти расширяет старый.\n");

    _malloc(FIRST_BLOCK_SIZE);
    printf("Heap after first allocation\n");
    debug_heap(stdout, heap);
    _malloc(EXTENDING_BLOCK_SIZE);
    printf("Heap after extending\n");
    debug_heap(stdout, heap);
    printf("Test 4 passed!\n\n\n");
}

void no_memory_another_place_allocating(void* heap) {
    printf("<------- TEST 5 ------->\n");
    printf("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");

    printf("Heap before\n");
    debug_heap(stdout, heap);
    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void* allocated = _malloc(BLOCK_FOR_ANOTHER_PLACE_SIZE);

    if (!allocated) {
        printf("Allocation failed!\n");
        return;
    }

    printf("Heap after allocating\n");
    debug_heap(stdout, heap);

    printf("Test 5 passed!\n\n\n");
}

void start_tests() {
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    simple_allocation(heap, FIRST_BLOCK_SIZE);
    free_one_block(heap);
    free_two_blocks(heap);
    new_region_extends_old_region(heap);
    no_memory_another_place_allocating(heap);
}